\section{The Weak $3$-Flow Conjecture for $k=6$}

Let $G = (V,E)$ be a graph. In this section we will not allow graphs to have loops, however, since a loop at $v$ contributes zero to $\hat{f}(v)$ for any flow $f$ this assumption will not affect our conclusions. Under an orientation $\Omega$ of $G$, we say that the \emph{in-degree} of a vertex $v$ is $d^-(v) = |\delta^-(v)|$ and that the \emph{out-degree} of $v$ is $d^+(v) = |\delta^+(v)|$. We can generalise the notion of degree to sets $A \subseteq V$, writing $d(A)$ for the number of edges $e \in E$ with exactly one end in $A$ (for loopless graphs this agrees with our original definition for the degree of $A$ a singleton). In addition, for sets $A,B \subseteq V$, we will write $d(A,B)$ for the number of edges with one end in $A$ and the other end in $B$.  

Given a map $f$ defined on $E$ we define the \emph{weighted in-degree} $w^-(v)$ and \emph{weighted out-degree} $w^+(v)$ of a vertex $v$ due to $f$ to be
$$w^-(v) = \sum_{e \in \delta^-(v)} f(e) \quad \text{ and } \quad w^+(v) = \sum_{e \in \delta^+(v)} f(e) $$
respectively.

Finally, we say that an orientation $\Omega$ of $G$ is a \emph{Tutte-orientation} if, for each vertex $v \in V$, the indegree of $v$ is congruent to the outdegree modulo 3, and that $G$ \emph{admits all generalized Tutte-orientations} if for every integer function $p(x)$ on $V$ there is an orientation of $G$ such that $p(x) \equiv d^+(x) \pmod 3$. For brevity we will write $x \equiv_k y$ for $x \equiv y \pmod k$.

The following theorem demonstrates that the 3-Flow Conjecture is equivalent to the statement that all $4$-edge-connected graphs have a Tutte-orientation. 

{\thm $G$ has a Tutte-orientation if and only if it has a nowhere-zero 3-flow.}
\begin{proof}
Suppose first that $G$ has a Tutte-orientation. Under such an orientation we have $d^+(v) \equiv_3 d^-(v)$ for all $v \in V$, so that $f:E \to \Z$ defined to be the constant map $f(e) = 1$ satisfies $w^+(v) \equiv_3 w^-(v)$ for all $v$. For an orientation $\Omega$ of $G$ and a map $f: E \to \Z$ with image $f(E) \subseteq \{1,2\}$ satisfying $w^+(v) \equiv_3 w^-(v)$ for all $v \in V$, define $P$ to be the set of vertices with $w^+(v) > w^-(v)$ and $Q$ the set of vertices with $w^+(v) < w^-(v)$. Let $\Omega$ and $f$ be chosen as above such that $\sum_{p \in P} w^+(p)$ is minimal. Since every edge contributes to the in-degree of one vertex and to the out-degree of one vertex we must have
$$\sum_{v \in V} w^+(v) = \sum_{v \in V} w^-(v)$$
In order to achieve this balance we must also have $\sum_{p \in P} w^+(p) = \sum_{q \in Q} w^-(q)$. It follows from this that if $P$ is empty then $Q$ must also be empty so that $f$ is a nowhere-zero 3-flow and we are done. Thus we may assume $P$ is non-empty. 
\renewcommand{\qedsymbol}{$\star\star\star$}
\end{proof}

\newpage
{\thm\label{thom1} Suppose that $G = (V,E)$ and $p : V \to \Z$ satisfy
\begin{enumerate}[(i)]
\item $|V| \geq 3$
\item $1 \leq d(z_0) \leq 11$
\item Each non-empty set $A \subseteq V \setminus \{z_0\}$ with $|V \setminus A| > 1$ has $d(A) \geq 6 + t(A)$
\item The sum of $p(v)$ over all $v \in V$ is congruent to $|E|$ modulo 3
\item $G - z_0$ has no problematic bridges.
\end{enumerate}
Then given an orientation of the edges incident to $z_0$ and assuming $p(z_0) = d^+(z_0)$, we can direct all edges not incident with $z_0$ such that $d^+(v) \equiv_3 p(v)$ for all $v \in V$.}

\begin{proof}
The proof is by induction on the number of edges of $G$. We suppose for contradiction, that $G$ is a minimal counter-example to the theorem.

Suppose that $G - z_0$ were disconnected. Let $C_1, \ldots, C_n$ be the components $(n \geq 2)$. In the whole graph $G$, $(iii)$ gives $d(V(C_i)) \geq 6$ for each $i \in [n]$. Since there are no edges between distinct connected components all of these edges must go to $z_0$. But then $d(z_0) \geq 6n \geq 12$, contradicting $(ii)$. Thus $G-z_0$ is connected, and it follows that $G$ is also connected. 

We now proceed with six claims which will lead us to a proof of the theorem.

{\clm\label{c1} For any two vertices $x,y$ distinct from $z_0$ there is at most one edge between $x$ and $y$. \note{Label claims within theorem or on their own?}}
\begin{proof}
Suppose for contradiction that there are $q \geq 2$ edges joining $x$ and $y$. Suppose first that $|V| = 3$. We can certainly orient the edges between $x$ and $y$ such that $d^+(x) \equiv_3 p(x)$. Then, recalling that $d^+(z_0) = p(z_0)$ by definition, the condition follows automatically at $y$ since
\begin{align*}
d^+(y) &= |E| - d^+(x) - d^+(z_0)\\
&\equiv_3 \sum_{v \in V} p(v) - p(x) - p(z_0)\tag{by $(iv)$}\\
&= p(y).
\end{align*}

We may suppose then that $|V| > 3$. In this case we contract $\{x,y\}$ to a new point $z$ and set $p(z) = p(x) + p(y) - q$. We call the new graph obtained by this process $G' = (V', E')$. To apply the  induction hypothesis to $G'$ we check each of the conditions:
\begin{enumerate}[$(i)$]
\item As we contracted $x$ and $y$ to a single point $z$, $|V'| = |V| - 1 \geq 3$.
\item Since $d(z_0)$ is unaffected by the contraction we still have $1 \leq d(z_0) \leq 11$.
\item By $(iii)$ in $G$ we have $d(\{x,y\}) \geq 6 + t(\{x,y\})$, therefore $d(z) \geq 6 + t(\{x,y\}) = 6 + t(z)$ where the final equality follows from the definition of $t(A)$ for a set of vertices $A$.
\item \parbox[t]{0.5\textwidth}{\vspace*{-2.2em}\begin{flalign*}
\sum_{v \in V'} p(v) &= \sum_{v \in V} p(v) - p(x) - p(y) + p(z) &\\
&= \sum_{v \in V} p(v) - q &\\
&\equiv_3 |E| - q & \tag{by $(iv)$ for $G$}\\
&= |E'| &
\end{flalign*}}
\item A problematic bridge in $G'$ would also be one in $G$, thus there can be no problematic bridges.
\end{enumerate}
With these conditions met we can, by induction, find an orientation on the edges of $G' - z_0$ such that $d^+(v) \equiv_3 p(v)$ for all $v \in V'$. This naturally gives an orientation of the edges of $G$, except those between $x$ and $y$, such that $d^+(v) \equiv p(v)$ for all $v \in V \setminus \{x,y,z_0\}$. If we orient the $q$ edges between $x$ and $y$ such that $d^+(x) \equiv_3 p(x)$ then since 
$$d^+(x) + d^+(y) = d^+(z) + q \equiv_3 p(z) + q = p(x) + p(y)$$
we must also have $d^+(y) \equiv_3 p(y)$ as required. This contradicts that $G$ is a counterexample and so there must be at most one edge between $x$ and $y$.
\end{proof}

{\clm\label{c2} $|V| > 4$}
\begin{proof}
We show that if $G$ has $|V| \leq 4$ then it cannot be a counterexample to the theorem.
Suppose first that $|V| = 3$. Since $G - z_0$ is connected there must be at least one edge between the other two vertices $x$ and $y$. By Claim \ref{c1} there is exactly one edge, which is a bridge in $G-z_0$. By $(v)$, it must not be a problematic bridge and so we can orient it so that $d^+(x) \equiv_3 p(x)$ and $d^+(y) \equiv_3 p(y)$. Thus $G$ is not a counterexample.

Now suppose $|V| = 4$. By Claim \ref{c1} each vertex $v \in V \setminus \{z_0\}$ must have degree at most two in $G-z_0$. But by $(iii)$, each $v$ also must have $d(v) \geq 6$ in $G$. Therefore, each such $v$ must have at least four edges to $z_0$. Then $d(z_0) \geq 12$, contradicting $(ii)$ and so again $G$ cannot be a counterexample.
\end{proof}

{\clm\label{c3} If $A \subseteq V$ does not contain $z_0$ and $|V \setminus A| > 1$, then either $d(A) \geq 12$ or $|A| = 2$ and the elements of $A$ have degrees $6,6$ or $6,7$.}
\begin{proof}
Suppose that $d(A) < 12$. Form a new graph $G'= (V',E')$ by contracting $A$ to a new vertex $z$. Let $p(z) = p(A)$. By a generalisation of the arguments of Claim \ref{c1}, $G'$ satisfies $(i)-(v)$ and so we can use induction to get an orientation $\Omega'$ for the edges $e \in E'$ such that $d^+(v) \equiv_3 p(v)$ for all $v \in V'$. 

Now consider the graph $G'' = (V'',E'')$ formed by contracting $V \setminus A$ to a new point $z_0'$. Orient the edges incident to $z_0'$ according to the orientation $\Omega'$ gives the corresponding edges from $v \setminus A$ to $z$ in $G'$. Then Theorem \ref{thom1} applies to $G''$ with $z_0'$ used for $z_0$.

If $G''$ satisfies $(i) - (v)$ then we can use induction to obtain an orientation $\Omega''$ of $G''$ such that $d^+(v) \equiv_3 p(v)$ for all $v \in V''$. Combining $\Omega'$ and $\Omega''$ would then give an orientation $\Omega$ of $G$ with $d^+(v) \equiv_3 p(v)$ for all $v \in V$, contradicting that $G$ is a counterexample to Theorem \ref{thom1}.

Thus we may assume that $G''$ does not satisfy $(i) - (v)$. Here again, similar arguments to those used in Claim \ref{c1} show that $G''$ must satisfy $(i) - (iv)$ and therefore $G''$ must not satisfy $(v)$. That is to say: there is a bridge $b$ in $G(A)$ which becomes problematic when we direct the edges from $z_0'$. We claim that then $|A| = 2$. Suppose rather that $|A| > 2$. Then there is a component $A_1$ of $G(A) - b$ with $|A_1| > 1$. If $A_2$ is the other component of $G(A) - b$, then by $(iii)$ we have $d(A_2) \geq 6$ and thus, recalling that $d(A) < 12$, we must have $d(A_1) \leq 7$. To obtain a contradiction we now run the above argument with $A_1$ for $A$.

Firstly, contracting $A_1$ we obtain by induction an orientation of the edges of $G$ except those contained in $G(A_1)$ satisfying $d^+(v) \equiv_3 p(v)$ for all $v \in V \setminus A_1$. Next we contract $V \setminus A_1$ to a single point $z_0''$. Again we can apply induction (with $z_0''$ for $z_0$) unless $A_1$ has a problematic bridge. However, if $A_1$ had a bridge $b_1$ then by $(iii)$ each of the components of $A_1 - b_1$ would have degree at least six in $G$, so that $d(A_1) \geq 10$. This contradicts that $d(A_1) \leq 7$, so $A_1$ cannot have a bridge. In particular, $A_1$ cannot have a problematic bridge and so we can use induction to obtain an orientation of the edges in $G(A)$, and hence on all edges of $G$ such that $d^+(v) \equiv_3 p(v)$ for all $v \in V$. This in turn contradicts that $G$ is a counterexample, and so we must indeed have $|A| = 2$.

Finally, as $d(A) < 12$ and each $a \in A$ has $d(a) \geq 6$ there must be at least one edge between the two elements of $A$. By Claim \ref{c1} there must be exactly one edge between them so that 
$$\sum_{a \in A} d(a) = d(A) + 2 < 14$$
and hence the elements of $A$ must have degrees $6,6$ or $6,7$.
\end{proof}

{\clm\label{c4} Every vertex $x$ distinct from $z_0$ has at least two neighbours and satisfies $d(x) = 6 + t(x)$. In particular, $d(x) = 6,7,8$ or $9$.}
\begin{proof}
A PROPERTY\note{refer to property in discussion of $t(A)$} of the function $t$ is that $t(x)$ and $d(x)$ have the same parity. Also, by $(ii)$ and $(iii)$ every $v \in V$ has $d(v) \geq 1$ and so, as $G$ is loopless, at least one neighbour. Therefore, we may suppose for a contradiction that either $x$ has exactly one neighbour or $d(x) \geq 6 + t(x) + 2$. We shall refer to this condition as ($\dagger$). We assume further that $x$ has maximal degree under this constraint.

Suppose that $x$ has a unique neighbour, $z$. By $(iii)$ applied to $x$ there are at least six edges from $x$ to $z$. Applying $(iii)$ to $\{x,z\}$ if $z \neq z_0$ and to $V \setminus \{x,z\}$\note{commas?} if $z = z_0$, there must also be at least six edges from $z$ to vertices other than $x$. Putting this together, $d(z) \geq d(x) + 6 \geq 12$. If $z = z_0$ then this contradicts $(ii)$, otherwise, since $t(z) \leq 3$ we have $d(z) \geq 12 \geq 6 + t(z)$, hence $z$ satisfies ($\dagger$) contradicting the maximality of $d(x)$.

Thus we may suppose that $x$ has at least two neighbours and $d(x) \geq 6 + t(x) + 2$. Let $y,z$ be distinct neighbours of $x$. Form the graph $G' = (V',E')$ by deleting one edge between $x$ and $y$ and one between $x$ and $z$ and adding an edge between $y$ and $z$ (lifting $xy$ and $xz$)\note{define lifting}. We then want to use induction on $G'$, with $p'$ agreeing with $p$ at all vertices except $x$ where $p'(x) = p(x) - 1$, to obtain a contradiction. We check the conditions $(i) - (v)$:
\begin{enumerate}[$(i)$]
\item Immediate since $V' = V$.
\item The formation of $G'$ from $G$ only changes the degree of $x$. Since $x \neq z_0$ by assumption, then $d_{G'}(z_0) = d_G(z_0)$ and so $1 \leq d_{G'}(z_0) \leq 11$.
\item If $A$ is a singleton other than $x$ then this follows by the observation above. For $A = \{x\}$ we have $d_{G'}(x) = d_G(x) - 2 \geq 6 + t(x)$ by assumption. If $|A| > 1$ then by Claim \ref{c3} we have (at worst) $d_G(A) \geq 10$. Since we deleted two edges to form $G'$, then $d_{G'}(A) \geq 8$ and, as $t(A) \leq 3$ and $t(A)$ and $d(A)$ have the same parity, it follows that $d_{G'}(A) \leq 6 + t(A)$.
\item By construction $G'$ has one fewer edge than $G$, and by the definition of $p'$ the sum of $p'$ over all vertices of $G'$ is one less than the sum of $p$ over all vertices of $G$. Thus
$$\sum_{v \in V'}p'(v) = \sum_{v \in V} p(v)\ -1 \equiv_3 |E| - 1 = |E'|.$$
\item If $G' - z_0$ had a bridge then we could separate the vertices of $G' - z_0$ into two sets $A$ and $B$ with only one edge between them in $G'$. Thus there can be at most three edges between $A$ and $B$ in $G$. As $d(z_0) \leq 11$ we may assume that there are at most five edges from $z_0$ to $A$. Thus $d(A) \leq 8$ so, by Claim \ref{c3}, $A$ must be a singleton, $a$. By $(iii)$, $d(a) \geq 6$ and so there must be at least three edges between $z_0$ and $a$. Hence there are at most eight edges from $z_0$ to $B$ and so $d(B) \leq 11$. However, by Claim \ref{c2} we must have $|B| > 2$, and then Claim \ref{c3} gives $d(B) \geq 12$. This gives a contradiction and so $G - z_0$ cannot have a problematic bridge.
\end{enumerate}

Now, by induction, $G'$ has an orientation $\Omega'$ such that $d^+(v) \equiv_3 p'(v)$ for all $v \in V$. If $\Omega'$ orients the added edge from $y$ to $z$, then form $\Omega$, an orientation on $G$, by orienting the edges deleted to form $G'$ from $y$ to $x$ and from $x$ to $z$. Otherwise, direct these edges from $x$ to $y$ and from $z$ to $x$. Under the orientation $\Omega$ we will then have $d^+(v) \equiv_3 p(v)$ for all $v \in V$, contradicting that $G$ is a counterexample. This proves the claim.

\end{proof}
\newpage
{\clm\label{c5} Every vertex $x$ distinct from $z_0$ has less than ${d(x)}/{2}$ edges joining $x, z_0$.}
\begin{proof}
Fix $x$ distinct from $z_0$ and let $m$ be the number of edges between $x$ and $z_0$. Let $A = V \setminus \{x,z_0\}$. By Claim \ref{c2}, $|A| > 2$. Therefore, by Claim \ref{c3}, $d(A) \geq 12$. Then by $(ii)$ we have
$$12 \leq d(A) = d(z_0) + d(A) - 2m \leq 11 + d(A) - 2m. $$
Rearranging gives $2m \leq d(x) - 1$ so that $m < d(x) / 2$ as required.
\end{proof}

{\clm\label{c6} If $x$ is a vertex distinct from $z_0$, then $t(x) > 0$.}
\begin{proof}
If $t(x) = 0$ then by Claim \ref{c4} we have $d(x) = 6$. We will construct a new graph by successively lifting edges incident to $x$ (as shown in Figure \ref{gprime}). Recall that in order to lift edges incident to $x$ we require two edges with distinct endpoints. We must show that the desired lifts are possible. By Claim \ref{c1}, there can only be multiple edges from $x$ to $z_0$, if $x$ has any multiple edges at all. However, by Claim \ref{c5}, there are less than $d(x)/2$ edges between $x$ and $z_0$ and thus we can lift each of those with edges from $x$ to vertices other than $z_0$. We can then perform the remaining lifts arbitrarily and remove $x$. We shall refer to the graph obtained by this process as $G' = (V',E')$, where $V' = V \setminus \{x\}$. In addition, we orient the edges incident to $z_0$ in $G'$ as they were oriented in $G$ - directing any new edges formed by lifting edges $z_0x$ into or out of $z_0$ according to whether $z_0x$ was directed into or out of $z_0$ in $G$ - so that $d_{G'}^+(z_0) = d_G^+(z_0) = p(z_0)$.

\input{gprime}

We will want to apply induction to $G'$ and therefore we must check that it satisfies the conditions $(i) - (v)$. 
As in the previous claims conditions $(i)$ and $(ii)$ are immediate. For $(iv)$, first observe that lifting three pairs of edges at $x$ reduces the number of edges by exactly three so that $|E| \equiv_3 |E'|$. Secondly, by PROPERTY $2p(x) \equiv_3 t(x) \pm d(x) = 0$ so that $p(x) \equiv_3 0$ and hence $\sum_{v \in V}p(v) \equiv_3 \sum_{v \in V'}p(v)$. Thus $(iv)$ is inherited from $G$ and it remains to show that $G'$ satisfies $(iii)$ and $(v)$.

We begin by checking $(iii)$. Let $A'$ be a non-empty subset of $V'$ such that $z_0 \not\in A'$ and $|V' \setminus A'| > 1$. Considering $A'$ and $A' \cup \{x\}$ as vertex sets in $G$, denote by $A$ the set with lowest degree. We claim that $d_G(A) \leq d_{G'}(A') + 2$. 

Let $X = \{x\} \cup \Gamma_G(x)$, where $\Gamma_G(x)$ is the set of neighbours of $x$ in $G$. \linebreak \note{Does this need to be said? Should I just state it as a fact that we only need to consider $A' \subseteq X$?} If $A'$ is disjoint from $X$ then the lifts performed in the formation of $G'$ will neither create nor remove any edges in or out of $A'$ so that the degree of $A'$ is unchanged by the process: $d_{G'}(A) = d_G(A)$. In addition, if $A' = B \cup C$ for $B$ disjoint from $X$ and $C \subseteq X$, then $d_{G'}(A) = d_{G'}(B) + d_{G'}(C) - d_{G'}(B,C)$ where $d_{G'}(B) = d_G(B)$ by the above argument and, as the lifts only change edges within $X$ (and hence within $C$), the number of edges between $B$ and $C$ is unchanged: $d_{G'}(B,C) = d_G(B,C)$. Thus $d_G(A) - d_{G'}(A) = d_G(C) - d_{G'}(C)$. It therefore suffices to consider only the cases where $A' \subseteq X$.

Each vertex $v \in \Gamma_{G}(x)$ distinct from $z_0$ is incident to exactly one edge in $G'$. As shown in Table \ref{degtab}, if only one end of this edge lies in $A$ then the lift that formed that edge does not affect the degree of $A$ whether $x$ is chosen in $A$ or not. If neither end of the edge lies in $A$ then when $x \in A$ the degree of $A$ in $G$ is two more than that in $G'$ since the edges from $x$ to the two ends are edges out of $A$. Similarly, if both ends of the edge are in $A$ then the edge is not an edge out of $A$ in $G'$, but the edges from both ends to $x$ are edges out of $A$ in $G$, so that the lift decreases the degree of $A$ by 2. Since there are three edges in $G'(\Gamma_G(x))$, there is either at most one edge with both ends in $A$ or at most one edge with neither end in $A$. In the former case $d_G(A') \leq d_{G'}(A') + 2$, and in the latter $d_G(A' \cup \{x\}) \leq d_{G'}(A') + 2$. As $A$ was chosen from $A'$ and $A' \cup \{x\}$ with least degree, then we certainly have $d_G(A) \leq d_{G'}(A') + 2$.

\begin{table}[h]
\centering
\begin{tabular}{ccccc}
 & \multicolumn{2}{c}{$A' \cup \{x\}$} & \multicolumn{2}{c}{$A'$}\\
 $\cdot - \cdot$ & $\cdot = \circ = \cdot$ & $+2$ & $\cdot - \cdot - \cdot$ & $+0$ \\
 $\cdot = \circ$ & $\cdot = \circ - \circ$ & $+0$ & $\cdot - \cdot = \circ$ & $+0$ \\
 $\circ - \circ$ & $\circ - \circ - \circ$ & $+0$ & $\circ = \cdot = \circ$ & $+2$
\end{tabular}
\caption{Comparing $d_G$ and $d_{G'}$}\label{degtab}
\end{table}

Now by Claim \ref{c3} we must have $d_G(A) \geq 10$ and therefore $d_{G'}(A') \geq 8$. Then, since $d_{G'}(A')$ and $t(A',G')$ have the same parity, $d_{G'}(A) \geq 6 + t(A',G')$ and so $G'$ satisfies $(iii)$.

Suppose now that $G' - z_0$ has a bridge. Then we can partition $V' \setminus \{z_0\}$ into two non-empty sets $A$ and $B$ with a single edge between them. Without loss of generality we may assume that $G'$ has at most five edges from $z_0$ to $A$. Then $d_{G'}(A) \leq 6$ so that $A$ must be a singleton $a$ - otherwise by the argument verifying $(iii)$ we would have $d_{G'}(A) \geq 8$. By $(iii)$, $a$ must have degree at least six, and so there must be at least (and hence exactly) five edges from $z_0$ to $a$. Then by $(ii)$ there can be at most six edges from $z_0$ to $B$, so that $d_{G'}(B) \leq 7$. But then $B$ must also be a singleton and hence $|V| =  |V'| + 1 = (|A| + |B| + 1) + 1 = 4$. This contradicts Claim \ref{c2} and so $G'$ cannot have a (problematic) bridge.

We can now apply induction to $G'$ to obtain an orientation satisfying the conclusion of Theorem \ref{thom1}. From this we can construct an orientation on $G$ by directing the edges incident to $x$ such that if the lifted edge $y_1y_2$ in $G'$ is directed from $y_1$ to $y_2$ then the edges $y_1x$ and $y_2x$ in $G$ should be directed from $y_1$ to $x$ and from $x$ to $y_2$ respectively. Then for each $v \in V'$ we have $d_G^+(v) = d_{G'}^+(v) \equiv_3 p(v)$ and at $x$ we have $d_G^+(x) = 3 \equiv_3 p(x)$ so that $G$ satisfies Theorem \ref{thom1}. This contradicts that $G$ is a counterexample, and so proves the claim.

\end{proof}

With Claims \ref*{c1} - \ref*{c6} in place, we now proceed to prove Theorem \ref{thom1}. Combining Claims \ref{c4} and \ref{c6} we see that any vertex $x$ distinct from $z_0$ must have degree 7,8 or 9. In particular, the second disjunct of Claim \ref{c3} cannot occur and hence we can strengthen the result of that claim as follows:
{\clmn[\ref*{c3}$\boldsymbol{^*}$]\mylabel{c3plus}{\ref*{c3}$\boldsymbol{^*}$} If $A \subset V$ does not contain $z_0$ and $|V \setminus A| > 1$ then $d(A) \geq 12$.}\\[-8pt]

Now suppose that $x$ is a vertex distinct from $z_0$ such that the number $m$ of edges between $x$ and $z_0$ is minimal. Assume that $t(x) = i$ where $1 \leq i \leq 3$. By Claim \ref{c4}, $d(x) = 6 + i$ and by Claims \ref{c1} and \ref{c5} it follows that $x$ has $n > 3 + i/2 > 3$ neighbours other than $z_0$. Thus $|V \setminus \{z_0\}| \geq n+1$ and so by $(ii)$ and the minimality of $m$:
\begin{equation*}\label{m}
m \leq \frac{d(z_0)}{n+1} \leq \frac{11}{4}. \tag{$\star$}
\end{equation*}
Since $m$ is an integer, then $m \leq 2$ so that $n = d(x) - m \geq 7-2 = 5$. Feeding this back into (\ref{m}) gives $m \leq 1$ and hence $n \geq 6$.

We will assume that $x$ is negative - otherwise an analogous argument to the one given below will work. By the results of the previous paragraph, $x$ has at least six distinct neighbours other than $z_0$. Therefore $x$ has either at least $i$ positive neighbours or at least $6 - i$ negative neighbours.

In the first case, pick $i$ positive neighbours of $x$ and direct the edge from each towards $x$. For each such neighbour $v$, deleting the edge $vx$ reduces both $t(v)$ and $d^+(v)$ by one, therefore we define $p'(v) = p(v) - 1$. In the second case, choose $6-i$ negative neighbours of $x$ and direct the edge from each away from $x$. For each of these neighbours $w$, deleting $wx$ reduces $t(w)$ by one, but does not change $d^+(w)$. Therefore we define $p'(w) = p(w)$. 

In either case, since $m \leq 1$ there are no multiple edges incident to $x$, and so having deleted all of the edges $vx$ (or $wx$) we can lift all remaining edges incident to $x$ (note that since $d(x)$ and $t(x)$ have the same parity there must be an even number of edges remaining) and remove $x$. We call this graph $G'= (V',E')$. We now apply induction to $G'$ with $p'$ differing from $p$ only as above.

In order to apply induction we must check that the conditions $(i) - (v)$ hold for $G'$ and $p'$. $(i)$ and $(ii)$ are immediate. For condition $(iv)$ we must check each of the above cases for $G'$. Note first that $d(x) = 6 + i \equiv_3 i$. Further, since $x$ is negative, PROPERTY gives $2p(x) \equiv_3 d(x)-i \equiv_3 0$ and so, in either case, $p(x) \equiv_3 0$. In the first case, we remove $i$ edges and then lift the remaining six edges, thus $|E'| = |E| - 3 - i \equiv_3 |E| - i$ and
$$\sum_{v \in V'} p'(v) = \sum_{v \in V} p(v)\ - p(x) - i \equiv_3 |E| - i = |E'|.$$
In the second case, we remove $6-i$ edges and then lift the remaining $2i$ edges. Thus $|E'| = |E| - 6 \equiv |E|$ and
$$\sum_{v \in V'} p'(v) = \sum_{v \in V} p(v)\ - p(x) \equiv_3 |E| \equiv_3 |E'|.$$
In either case, then, $(iv)$ is satisfied. It remains to show $(iii)$ and $(v)$.

For $(iii)$ consider a set $A' \subseteq V'$ such that $z_0 \not \in A'$ and $|V' \setminus A'| > 1$. First, if $A'$ is a singleton $a$, then $d_{G'}(a) = d_G(a)$ unless $a$ is incident to one of the edges deleted in the formation of $G'$, in which case $d_{G'}(a) = d_G(a)$. In this case, thought, we noted that the type of $a$ is also decreased by one, so that $(iii)$ is still satisfied. Thus we may suppose that $A'$ is not a singleton, in which case we choose $A = A'$ or $A' \cup \{x\}$ with minimal degree as in Claim \ref{c6}. Locally to $x$\note{where $x$ was, how to say this?}, $G'$ consists of single vertices to which the edge to $x$ was deleted and pairs of edges joined by a lifted edge. 

\begin{table}[h]
\centering
\begin{tabular}{cccccc}
 && \multicolumn{2}{c}{$A' \cup \{x\}$} & \multicolumn{2}{c}{$A'$}\\
 (a) & $\cdot - \cdot$ & $\cdot = \circ = \cdot$ & $+2$ & $\cdot - \cdot - \cdot$ & $+0$ \\
 (b) & $\cdot = \circ$ & $\cdot = \circ - \circ$ & $+0$ & $\cdot - \cdot = \circ$ & $+0$ \\
 (c) & $\circ - \circ$ & $\circ - \circ - \circ$ & $+0$ & $\circ = \cdot = \circ$ & $+2$\\
 (d) & $\cdot$ & $\cdot = \circ$ & $+1$ & $\cdot - \cdot$ & $+0$ \\
 (e) & $\circ$ & $\circ - \circ$ & $+0$ & $\circ - \cdot$ & $+1$
\end{tabular}
\caption{Comparing $d_G$ and $d_{G'}$}\label{degtab2}
\end{table}

Table \ref{degtab2} shows the difference between $d_G(A)$ and $d_{G'}(A')$ for the two cases of $A$ and different choices of $A'$. Either fewer than half the vertices neighbouring $x$ in $G$ are (a) or (d), or fewer than half are (c) or (e). Choosing $A$ equal to $A' \cup \{x\}$ or $A'$ respectively for each of these cases gives
$$d_G(A) \leq d_{G'}(A') + \frac{d_G(x)}{2},$$
therefore, since degrees are integral,
$$d_G(A) \leq d_{G'}(A') + 4.$$
By Claim \ref{c3plus} we then have
$$d_{G'}(A') \geq d_G(A) - 4 \geq 12 - 4 = 8.$$
Thus $(iii)$ is satisfied since $d(A')$ and $t(A')$ have the same parity. Finally, we must show that $G'$ has no problematic bridge.

Suppose that $G' - z_0$ has a bridge. Then we can partition $V'$ into two sets $A$ and $B$ with a single edge between them. If either $A$ or $B$ is a singleton $v$ then all edges incident to $v$ in $G'$ except one must be incident to $z_0$, and hence all edges except at most two edges incident to $v$ in $G$ must be incident to $z_0$. However, this contradicts Claim \ref{c5} since $d_G(v) \geq 6$. Thus $A$ and $B$ are not singletons and so by Claim \ref{c3plus} 
$$24 \leq d_G(A) + d_G(B).$$
Then since the edges out of $A$ and $B$ in $G$ are those to $z_0$, those to $x$ and the single edge between them we have
$$24 \leq d_G(A) + d_G(B) \leq d_G(z_0) + d_G(x) + 2 \leq 11 + 9 + 2 = 22.$$
This contradiction means that $G'$ cannot have a bridge, and hence we can apply induction to obtain an orientation of $E'$ with $p(v) \equiv_3 d_{G'}^+(v)$ for all $v \in V'$. Then orienting the deleted edges into $x$ as explained in the construction of $G'$ and the lifted edges as in the closing arguments of Claim \ref{c6} we obtain an orientation of $G$ satisfying the conclusion of the theorem. Hence $G$ is not a counterexample, and we have proved the theorem.

\end{proof}

\com{1}{{\thm Every 8-edge-connected graph $G$ has a nowhere-zero 8-flow.}
\begin{proof}

\end{proof}}