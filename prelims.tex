\chapter{Preliminaries}
\chaps{Preliminaries}

In this chapter we shall present some of the more basic graph theoretic results underpinning the results towards Tutte's flow conjectures presented later. Their proofs are included for completeness, however their statements alone are sufficient to follow the subsequent material in Chapters \ref{ch5flow}-\ref{ch3flow}. We begin with a number of additional definitions from general graph theory.

A \emph{path} in a graph $G$ is a sequence of distinct vertices $v_1 \ldots v_n$ such that $v_iv_{i+1} \in E$ for all $1 \leq i < n$. A \emph{cycle} in $G$ is a closed path: we write $v_1 \ldots v_n$ where $v_1 \ldots v_{n-1}$ is a path, $v_1 = v_n$ and $v_{n-1}v_n \in E$. In oriented graphs we shall also discuss \emph{directed paths} (resp. \emph{cycles}) where we also require that each edge in the path (resp. cycle) is directed from $v_i$ to $v_{i+1}$.

For a vertex $v$, the \emph{connected component} of $v$ is the largest subgraph $C_x$ of $G$ such that every vertex in $C_x$ can be reached from $x$ by a path in $G$. If $G$ has a single connected component then we say that $G$ is \emph{connected}, otherwise we say that $G$ is \emph{disconnected}. We also have measures of how strongly connected a graph $G$ is. We say that $G$ is $k$\emph{-connected} if there is no set $S \subseteq V$ with $|S| < k$ such that $G \setminus S$ is disconnected, and that $G$ is $k$\emph{-edge-connected} if there is no set $T \subseteq E$ with $|T| < k$ such that $G - T$ disconnected.

The first lemma of this chapter concerns the connectivity of cubic graphs. We say that a graph $G$ is \emph{cubic} if every vertex $v$ has $d_v = 3$.

{\lem\label{cubcon} Let $G$ be a cubic graph that is 3-edge-connected, then $G$ is 3-connected.}

\begin{proof}
First, suppose $G$ has a cut vertex $v$, then $G-v$ has at least two connected components. Say $G_1$ and $G_2$ are connected components of $G-v$. As $G$ is cubic,\linebreak $v$ has exactly one edge into one of the $G_i$. But this edge is a bridge in $G$, as removing it disconnects $G_1$ and $G_2$, which contradicts that $G$ is 3-edge-connected. 
Now, suppose that $X = \{u,v\}$ is a vertex cut in $G$ and let $G_1$ and $G_2$ be two connected components of $G-X$. If one of $u$ or $v$ has no edges into one of the $G_i$ then the other is a cut-vertex and we are in the previous case. Thus we may assume that both $u$ and $v$ have at least one edge into each of the $G_i$. If the edge $uv$ is present in $G$ then both $u$ and $v$ must have exactly one edge into each $G_i$. Then the two edges into $G_1$, say, would form a two edge cut, contradicting the 3-edge-connectedness of $G$. Otherwise, as $G$ is cubic, $u$ has exactly one edge into one of the $G_i$ and similarly $v$ has exactly one edge into one of the $G_i$. These two edges will then separate $G_1$ and $G_2$ and thus form a two edge cut of $G$, contradicting that $G$ is 3-edge-connected.
\end{proof}

The next few results are central to the study of nowhere-zero flows. 
%{\color{red}Say a bit more about them when you've thought of something useful and informative. Try to make it interesting and ensure to drag it on for long enough that the corollary gets forced onto the next page! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vulputate cursus arcu, id feugiat tortor tincidunt vitae. Donec lacinia enim a dolor ornare egestas. }

{\props\label{flowcor} Let $H$ be an abelian group, $G$ a graph and $e$ an edge of $G$ which is not a loop. Then there is a bijection between the nowhere-zero $H$-flows on $G/e$ and the nowhere-zero $H$-flows on $G$ and $G-e$.}

\begin{proof}
Let $e$ be an edge of $G$ which is not a loop. We shall use the following notation:
\begin{align*}
F &= \{f : f \text{ is a nowhere-zero $H$-flow on $G$}\}\\
F_1 &= \{f : f \text{ is a nowhere-zero $H$-flow on $G-e$}\}\\
F_2 &= \{f : f \text{ is a nowhere-zero $H$-flow on $G/e$}\}
\end{align*}
We claim that there is a bijection between $F_2$ and $F \sqcup F_1$. First, observe that a nowhere-zero $H$-flow on $G - e$ is the restriction of a flow on $G$ that is nowhere-zero except on $e$, where it must be zero in order to satisfy Kirchhoff's Law. Thus $F \sqcup F_1$ is the collection of all flows on $G$ that are nowhere-zero except possibly on $e$.

Let $g$ be a nowhere-zero $H$-flow on $G/e$. Construct a flow $f$ on $G$ as follows. Let $f(e') = g(e')$ for all edges $e' \in G/e$ and set $f(e) = 0$. Then $\hat{f}(v) = 0$ for all $v$ except possibly the two endpoints, $x$ and $y$, of $e$. Since $g$ is a flow we must have $\hat{f}(x) = -\hat{f}(y)$ and then we can choose $f(e) = \pm \hat{f}(x)$ (according to the direction of $e$) so that $f$ is a flow on $G$. In this way, $g$ defines a unique flow on $G$ which is nowhere-zero except possibly on $e$. Conversely, any nowhere-zero $H$-flow $f$ on either $G$ or $G-e$ defines a unique nowhere-zero $H$-flow on $G/e$ by taking the restriction of $f$ to the edge set $E \setminus e$. 
\end{proof}
\newpage
{\cor\label{subcount} $\Phi(G,H) = \Phi(G/e,H) - \Phi(G-e,H)$.}
\begin{proof}
Using the notation above we have $|F| = \Phi(G,H)$, $|F_1| = \Phi(G-e,H)$ and $|F_2| = \Phi(G/e,H)$. Then by the bijection $F_2 \leftrightarrow F \sqcup F_1$ we have as required
$$\Phi(G,H) = |F| = |F_2| - |F_1| = \Phi(G/e,H) - \Phi(G-e,H).$$
\end{proof}

{\props\label{bridgenoflow} Let $G$ be a graph with a bridge $b$. Then $\Phi(G,H) = 0$ for all $H$.}
\begin{proof}
Suppose that $G$ is a graph with a bridge $b$ and that $f$ is a flow on $G$. Let $G_1$ and $G_2$ be the components of $G-b$ and suppose that $b$ is directed into $G_1$. Further let $U = V(G_1)$. Since $f$ is a flow, $\hat{f}(U) = \sum_{u \in U} \hat{f}(u) = 0$. Then as $b$ is the only edge in or out of $U$ we must have $f(b) = \hat{f}(U) = 0$. Thus $f$ cannot be nowhere-zero.
\end{proof}


{\thm\label{anyh} Let $G$ be a graph and let $H_1,H_2$ be two abelian groups of order $k$. Then $G$ has a nowhere-zero $H_1$-flow if and only if it has a nowhere-zero $H_2$-flow. Moreover, $\Phi(G,H_1) = \Phi(G,H_2)$.}
\begin{proof}
We prove the strengthening that $\Phi(G,H_1) = \Phi(G,H_2)$. This clearly implies the if and only if statement. 

We use induction on the number of edges of the graph $G$. First, if the only edges of $G$ are loops then ascribing any non-zero element of $H_1$ (resp. $H_2$) to each edge will result in a nowhere-zero flow. Setting $l$ to be the number of loops, we then have $\Phi(G,H_1) = (k - 1)^l = \Phi(G,H_2)$. 

Now suppose that $G$ has some edge $e$ which is not a loop. By Corollary \ref{subcount} we have $\Phi(G,H_1) = \Phi(G/e,H_1) - \Phi(G-e,H_1)$. Each of $G/e$ and $G-e$ has one fewer edges than $G$ so that by induction we have $\Phi(G/e,H_1) = \Phi(G/e,H_2)$ and $\Phi(G-e,H_1) = \Phi(G-e,H_2)$. Using Corollary \ref{subcount} again we obtain:
\begin{align*}
\Phi(G,H_1) &= \Phi(G/e,H_1) - \Phi(G-e,H_1)\\
 &= \Phi(G/e,H_2) - \Phi(G-e,H_2) = \Phi(G,H_2).
\end{align*}
\end{proof}

Theorem \ref{anyh} enables us to study the existence of nowhere-zero $H$-flows for general abelian groups $H$ by studying the existence of nowhere-zero flows for simple groups such as the cyclic groups $\Z_n$. The final theorem in this chapter says that, likewise, questions about nowhere-zero $k$-flows are equivalent to questions about $\Z_k$-flows. This result is originally due to Tutte \cite{T5}, whilst the structure of our exposition here is due to Younger \cite{younger}. %Our proof of the theorem involves the manipulation of orientations and so for clarity we will often revert to using the notation $(G, \Omega)$ for a graph $G$ with orientation $\Omega$.

%Given an orientation $\Omega$, a \emph{directed path} is a path where also $\iota(v_iv_{i+1}) = v_i$ and $\tau(v_iv_{i+1})=v_{i+1}$ for all $i$. We define a \emph{directed cycle} similarly. 

{\thm\label{kzk} A graph $G$ has a nowhere-zero $k$-flow if and only if it has a nowhere-zero $\Z_k$-flow.}
\begin{proof}\let\qed\relax
For the forwards direction, a nowhere-zero $k$-flow can be viewed as a nowhere-zero $\Z_k$-flow just by taking the flow along each edge modulo $k$. Hence it remains to show that from a nowhere-zero $\Z_k$-flow we can obtain a nowhere-zero $k$-flow. We will shall make use of the following results.
\end{proof}

{\props\label{modkor} For a nowhere-zero $\Z_k$-flow $f$ on $(G,\Omega)$ and another orientation $\Theta$ of $G$, there is a nowhere-zero $\Z_k$-flow $f'$ such that 
$$f'(e) = \begin{cases} k - f(e) & e \in D\\ f(e) & e \not\in D\end{cases} $$ where $D \subseteq E$ is the set of edges on which $\Omega$ and $\Theta$ differ.}
\begin{proof}
Notice first that whenever $0 < x < k$ we have $0 < k-x < k$, so that $f':E \to \Z_k$ is nowhere-zero. Furthermore, since $y + x \equiv y - (k - x) \pmod k$, the contribution of a flow of $x$ into a vertex $v$ is equal$\pmod k$ to that of a flow of $k - x$ out of the vertex. Therefore, $0 = \hat{f}(e) \equiv \hat{f}'(e) \pmod k$, so that $f'$ is a nowhere-zero $\Z_k$-flow as required.
\end{proof}

We define the \emph{balance} of a $\Z_k$-flow $f$ at each vertex $v$ to be the net flow out of $v$ of the $\Z$-flow $f_\Z$ defined such that $f_\Z(e)$ is the least positive integer such that $f_\Z(e) \equiv f(e) \pmod k$. Since $f$ is a $Z_k$-flow the net outflow at $v$ is equal to zero modulo $k$ and so the balance at $v$ must also be zero modulo $k$. Relative to $f$ we say that $v$ is a \emph{balanced vertex} if $\hat{f}_\Z(v) = 0$, a \emph{plus vertex} if $\hat{f}_\Z(v) > 0$ and a \emph{minus vertex} if $\hat{f}_\Z(v) < 0$. The \emph{deviation} of a flow $f$ is the sum of the net outflows over all plus vertices. It follows from this definition that the deviation of $f$ is always non-negative and that whenever the deviation of $f$ is zero, $f_\Z$ is a $k$-flow.

{\lem\label{posneg} Let $f$ be a nowhere-zero $\Z_k$-flow on $(G,\Omega)$. Then for any plus vertex $v$ there is a directed path to a minus vertex.}
\begin{proof}
Let $v$ be a plus vertex in $G$ relative to $f$. Define $U_v$ to be the set of vertices $u$ of $G$ such that there is a directed path from $v$ to $u$. The number of edges directed out of $U_v$ must be zero otherwise there would be a directed path form $v$ to a vertex outside $U_v$. Therefore $\hat{f}_\Z(U_v) \leq 0$. Since $v$ is positive $\hat{f}_\Z(v) > 0$ so that $\hat{f}_\Z(U_v \setminus \{v\}) < 0$. Thus there must be some $w \in U_v \setminus \{v\}$ with $\hat{f}_\Z(w) < 0$. Then $w$ is a minus vertex and by definition of $U_v$ there is a directed path from $v$ to $w$ as required.
\end{proof}

\begin{proof}[Proof of Theorem \ref{kzk} continued.]\note{Diagram?}
Given a nowhere-zero $\Z_k$-flow $f$ on $(G, \Omega)$ we show by induction on the deviation of $f$ that $f$ can be converted into a nowhere-zero $k$-flow. If $f$ has zero deviation, then $f_\Z$ is a nowhere-zero $k$-flow and we are done. Otherwise, suppose $v$ is a plus vertex and that all $\Z_k$-flows with smaller deviation than $f$ can be converted into $k$-flows. By Lemma \ref{posneg} there is a directed path $P = p_0 \ldots p_n$ from $v$ to a minus vertex $w$. Define a new orientation $\Theta$ from $\Omega$ by reversing the orientation of each edge in $P$. Then we can define a $\Z_k$-flow $f'$ on $(G, \Theta)$ according to Proposition \ref{modkor}. At each vertex $x$ not in $P$ the balance of $f'$ at $x$ is unchanged from the balance of $f$ at $x$. For each vertex $p_i$ of $P$ other than $v$ and $w$ the contribution of the edges $p_{i-1}p_i$ and $p_ip_{i+1}$ to the balance at $p_i$ are increased and decreased by $k$ respectively so that overall the balance at $p_i$ is unchanged. Finally, at $v$ the balance is decreased by $k$, and at $w$ the balance is increased by $k$. Since $v$ and $w$ were positive and negative vertices respectively, then $\hat{f}_\Z(v) = n_vk$ and $\hat{f}_\Z(w) = -n_wk$ for strictly positive $n_v, n_w \in \N$. Therefore, for $f'$ we have $$\hat{f}_\Z(v) > \hat{f}'_\Z(v) = (n_v-1)k \geq 0 \quad \text{ and } \quad \hat{f}'_\Z(w) = -(n_w-1)k \leq 0$$
and hence the deviation of $f'$ is strictly less than the deviation of $f$. By the induction hypothesis, $f'$ can be converted into a $k$-flow, so $f$ can be converted into a $k$-flow and we are done.

%For each edge $e$ whose orientation differs between $\Omega$ and $\Theta$, the balance at $\iota_\Omega(e)$ is decreased by $k$ and the balance at $\tau_\Omega(e)$ is increased by $k$. (As shown in Figure \ref{balance}.) These changes cancel out each vertex of $P$ other than $v$ and $w$. At $v$ the balance is decreased by $k$ and at $w$ it is increased by $k$.

\end{proof}