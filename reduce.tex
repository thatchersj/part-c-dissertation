\section{An Initial Reduction}

Our first step is to characterise the counterexamples to Theorems \ref{eightflow} and \ref{sixflow}. In this section we will show that minimal counterexamples are simple, cubic and 3-connected. In order to achieve this characterisation we first prove a technical lemma due to Fleischner \cite{Flei}. We will make use of the following procedure.

{\proc For a vertex $v$ of a graph $G$ with incident edges $e_1, \ldots, e_n$ and $I \subseteq [n]$, we define the graph $G_I$ obtained from $G$ by \emph{splitting} the vertex $v$ to two vertices $v_I$ and $v_I'$ such that the edge $e_j$ of $G$ is incident to $v_I$ in $G_I$ if $j \in I$ and incident to $v_I'$ in $G_I$ otherwise.}

\input{splitvertex}\vspace*{-0.8em}

{\lem[Fleischner]\label{flei} Let $G = (V,E)$ be a connected bridgeless graph and let $v \in V$ have degree $d(v) \geq 4$. Then there are $vx,vy \in E$, edges incident to $v$, such that the graph obtained by removing these edges and adding a new edge $\ovE{xy}$ is connected and bridgeless.}
\begin{proof}
We tackle first the case where $v$ is a cutvertex in $G$. Let $G_1, \ldots, G_k$ be the connected components of $G-v$. As $G$ is connected there must be an edge from $v$ into each of the $G_i$, further, if there were only one such edge then it would be a bridge in $G$,  contradicting that $G$ is bridgeless, so there must be at least two edges from $v$ into each $G_i$. Let $vv_1, vv_2$ be edges in $G$ from $v$ to $v_i \in V(G_i)$. We claim that the graph $G'$ obtained by deleting these edges and adding a new edge $\ovE{v_1v_2}$ is connected and bridgeless.

\input{flei1}

The $G_i$ are connected components of $G-v$ and each connected to $v$ so that the whole graph $G'$ is connected. The new edge $\ovE{v_1v_2}$ is not a bridge as $G_1$ and $G_2$ are connected to $v$, and hence to each other in $G'-\ovE{v_1v_2}$, and the remainder of $G'$ is unchanged. None of the edges adjacent to $v$ can be bridges since for all $i \neq 1,2$ there are two edges connecting the connected component $G_i$ to $v$, and for $i = 1,2$ we have $G_1$ and $G_2$ connected by the edge $\ovE{v_1v_2}$ so that removing the single edge to $G_i$ does not disconnect $G'$. This leaves only edges within a connected component $G_i$. However, if one such edge were a bridge in $G'$ then it must be a bridge in $G$, contradicting that $G$ is bridgeless. Therefore $G'$ is a connected bridgeless graph as required.


In the case where $v$ is not a cut vertex, we consider the graphs resulting from two ways of splitting the vertex $v$. Suppose $e_1 = vv_1, e_2 = vv_2, \ldots, e_n = vv_n$ are the edges incident to $v$, by hypothesis we have $n \geq 4$. Consider the graphs $G_{12}$ and $G_{13}$ obtained by splitting $v$ with $I=\{1,2\}$ and $I=\{1,3\}$ respectively. We claim that one of $G_{12}$ or $G_{13}$ must be connected and bridgeless; the result then follows by taking $x=v_1$ and $y=v_i$ where $G_{1i}$ is connected and bridgeless.

Since $v$ is not a cut vertex, $G-v$ is connected. It follows that $e_i$ is not a bridge for each $i = 1, \ldots, n$. Thus if $b$ is a bridge in $G_{1i}$, then $b \in E(G-v)$. For such a bridge $b$, if $v_{1i}$ and $v_{1i}'$ were in the same component of $G_{1i} - b$ then $b$ would be a bridge in $G$, contradicting that $G$ is bridgeless. Thus, if there is a bridge $b$ in  $G_{1i}$, then $v_{1i}$ and $v_{1i}'$ must lie in different components of $G_{1i} - b$ .\\
\input{flei2}

Now suppose that $G_{12}$ and $G_{13}$ both contain bridges, $b_2, b_3$ respectively. We consider the graph $G^* = (G-v) - \{b_2, b_3\}$. Since deleting an edge increases the number of components by at most one, $G^*$ has at most three components.

On the other hand, removing $b_2$ separates $v_1$ and $v_2$ from $v_3$ and $v_4$ in $G-v$, and removing $b_3$ separates $v_1$ and $v_3$ from $v_2$ and $v_4$ in $G-v$, so that in $G^*$ we must have $v_1,v_2,v_3$ and $v_4$ in distinct connected components. This contradicts that $G^*$ has at most three connected components, and so at least one of $G_{12}$ and $G_{13}$ must be bridgeless.
\end{proof}


{\lem\label{sey2.1} For $k > 2$, let $G = (V,E)$ be a bridgeless graph with no nowhere-zero $k$-flow such that $G$ has $|V|+|E|$ minimal. Then $G$ is simple, cubic and 3-connected.}
\begin{proof}
Suppose there were a loop $e$ in $G$, let $G' = G - e$. Any bridge in $G'$ would also be a bridge in $G$, so $G'$ must be bridgeless. Therefore, by the minimality of $G$, there must be a nowhere-zero $k$-flow $f$ on $G'$. But then we can extend $f$ to a nowhere-zero $k$-flow on $G$ by setting $f(e) = l$ for some $0 < l < k$, contradicting that $G$ has no such flow. Thus $G$ contains no loops. Now suppose there are vertices $v,w \in V$ with multiple edges $e_1, e_2, \ldots, e_n$ between them and let $G'' = G/e_1$. Again any bridge in $G''$ would be a bridge in $G$ so that $G''$ is bridgeless and by the minimality of $G$ has a nowhere-zero $k$-flow $f'$. We will construct a flow $f$ on $G$ from $f'$. For all $e \in E \setminus \{e_1,e_2\}$, define $f(e)=f'(e)$. Since $k > 2$, we can either increase or decrease $f'(e_2)$ by one such that $|f'(e_2)|$ remains strictly between $0$ and $k$. If we may increase $f'(e_2)$ by one then define $f(e_2) = f'(e_2) + 1$ and
$$f(e_1) = \begin{cases} 1 & \text{if $e_1$ is oriented in the opposite direction to $e_2$} \\ -1 & \text{if $e_1$ is oriented in the same direction as $e_2$} \end{cases} $$
\note{need to translate to proof about $\Z_k$ flows to get this to work}
Otherwise, define $f(e_2) = f'(e_2) - 1$ and set
$$f(e_1) = \begin{cases} 1 & \text{if $e_1$ is oriented in the opposite direction to $e_2$} \\ -1 & \text{if $e_1$ is oriented in the same direction as $e_2$} \end{cases} $$
Thus (with respect to $f'$) $f$ fixes the flow through all vertices except $v$ and $w$ and our construction ensures that at $v$ and $w$ any change to the flow into the vertex is balanced by an equal and opposite change to the flow out of the vertex and vice versa. Thus, since $f'$ obeys Kirchhoff's Law, so too does $f$. In addition, $0 < |f(e)| < k$ for all $e \in E$, so $f$ is a nowhere-zero $k$-flow on $G$. This contradicts that $G$ has no such flow and so we have shown that $G$ must be simple.\pagebreak[1]

$G$ is 2-connected since if there were a cut-vertex $v$ such that $G-v$ had connected components $C_1, \ldots, C_n$, then by the minimality of $G$ each of the graphs $G_i = G[V(C_i) \cup \{v\}]$ has a nowhere-zero $k$-flow and combining all these would give a nowhere-zero $k$-flow on $G$. 

$G$ is $2$-edge-connected since it is bridgeless, we next show that it is also 3-edge-connected. If $\{e_1, e_2\}$ were a two-edge cut, then the graph $G/e_1$ obtained by contracting the edge $e_1$ would be connected and bridgeless and so, by the\linebreak minimality of $G$, would have a nowhere-zero $k$-flow, $f$. We have seen that $G$ is simple thus, by Proposition \ref{flowcor}, $f$ corresponds to a nowhere-zero $k$-flow on either the original graph $G$ or on the graph $G-e_1$. However,as $\{e_1,e_2\}$ is a two-edge cut in $G$, $e_2$ is a bridge in $G-e_1$ and so by Proposition \ref{bridgenoflow} $G-e_1$ cannot have a nowhere-zero $k$-flow. Thus $f$  must correspond to a nowhere-zero $k$-flow on $G$ contradicting that $G$ has no such flow, and hence $G$ is 3-edge-connected.

Next, suppose that $G$ has some vertex $v$ of degree greater than 3. Then by Lemma \ref{flei} we can find edges $vx$ and $vy$ such that the graph $G'$ obtained by deleting $vx$ and $vy$ and adding a new edge $\ovE{xy}$ is connected and bridgeless. By the minimality of $G$ this graph has a nowhere-zero $k$-flow. Without loss of generality we may assume that $\ovE{xy}$ is directed from $x$ to $y$, $vx$ from $x$ to $v$ and $vy$ from $v$ to $y$ (otherwise we may negate the flows along them to have the same effect). Then if $f'$ is a nowhere-zero $k$-flow on $G'$ we can define:
$$f(e) = \begin{cases} f'(e) & e \in E(G') \setminus \{xy\} \\ f'(xy) & e \in \{vx,vy\} \end{cases} $$
By construction, $f$ is nowhere-zero on $G$, bounded by $k$ and (since $f'$ is a flow) a flow except possibly at $v, x$ and $y$. However, there we have:
\begin{align*}
\hat{f}(v) &= \hat{f}'(v) + \hat{f}(vx) - \hat{f}(vy) \phantom{'}= \hat{f}'(v) + \hat{f}'(xy) - \hat{f}'(xy) = \hat{f}'(v) = 0\\
\hat{f}(x) &= \hat{f}'(v) - \hat{f}'(xy) + \hat{f}(vx) = \hat{f}'(v) - \hat{f}'(xy) + \hat{f}'(xy) = \hat{f}'(v) = 0\\
\hat{f}(y) &= \hat{f}'(v) + \hat{f}'(xy) - \hat{f}(vy) = \hat{f}'(v) + \hat{f}'(xy) - \hat{f}'(xy) = \hat{f}'(v) = 0
\end{align*}
Thus $f$ is a nowhere-zero $k$-flow on $G$, contradicting the choice of $G$, so that all vertices in $G$ have degree at most $3$. Since $G$ is 3-edge connected, all vertices in $G$ have degree at least $3$ and so we must have that all vertices in $G$ have degree exactly $3$, so that $G$ is cubic. Now, a cubic graph, 3-edge-connected graph is 3-connected by Lemma \ref{cubcon}, and so we are done. 
\end{proof}