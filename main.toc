\contentsline {chapter}{\chapternumberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\chapternumberline {2}Preliminaries}{5}{chapter.2}
\contentsline {chapter}{\chapternumberline {3}Flow-Colouring Duality}{10}{chapter.3}
\contentsline {section}{\numberline {3.1}Planar Graphs}{10}{section.3.1}
\contentsline {section}{\numberline {3.2}The Duality Theorem}{13}{section.3.2}
\contentsline {chapter}{\chapternumberline {4}The 5-Flow Conjecture}{15}{chapter.4}
\contentsline {section}{\numberline {4.1}An Initial Reduction}{16}{section.4.1}
\contentsline {section}{\numberline {4.2}$8$-Flow Theorem}{20}{section.4.2}
\contentsline {section}{\numberline {4.3}Seymour's $6$-Flow Theorem}{23}{section.4.3}
\contentsline {chapter}{\chapternumberline {5}The 4-Flow Conjecture}{24}{chapter.5}
\contentsline {section}{\numberline {5.1}The Petersen Graph}{24}{section.5.1}
\contentsline {section}{\numberline {5.2}Snarks}{25}{section.5.2}
\contentsline {chapter}{\chapternumberline {6}The 3-Flow Conjecture}{26}{chapter.6}
\contentsline {section}{\numberline {6.1}The Weak $3$-Flow Conjecture for $k=6$}{26}{section.6.1}
