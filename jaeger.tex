\section{$8$-Flow Theorem}

In this section, we will present a constructive proof of Jaeger and Kilpatrick's result that every bridgeless graph admits a nowhere-zero 8-flow. We first prove the following proposition. Note that a graph is \emph{even} if every vertex has even degree.

{\props\label{props:evensub} A graph $G$ has a nowhere-zero $2^p$-flow if and only if there exist even subgraphs $F_1, \ldots, F_p$ of $G$ such that $G = \bigcup_{\;i=1}^{\;p} F_i$. }
\begin{proof}
First, suppose that we have such subgraphs $F_1, \ldots, F_p$ as described above. Let $H = \Z_2 \times \cdots \times \Z_2$ be the direct sum of $p$ copies of $\Z_2$. Then we can define $f : E \to H$ such that
$$f(e)_i = \begin{cases} 1 & \text{if $e \in F_i$}\\ 0 & \text{otherwise.} \end{cases}$$ 
For any orientation $\Omega$ of $G$, since each $F_i$ is even we must have $\hat{f}(v)_i = 0$ for all vertices $v$ and indices $i$. Thus $f$ is a nowhere-zero $H$-flow. Since $|H| = 2^p$, applying Theorems \ref{anyh} and \ref{kzk} we find that $G$ has a nowhere-zero $2^p$-flow.

Conversely, suppose that $G$ has a nowhere-zero $2^p$-flow. Then Theorems \ref{anyh} and \ref{kzk} give us a nowhere-zero $H$-flow $f$ on $G$, with $H$ as defined above. Then for $1 \leq i \leq p$ define
$$E(F_i) = \{e \in E : f(e)_i = 1\}$$
and
$$V(F_i) = \{v \in V : \exists e \in F_i \text{ incident to } v\}.$$

Since $f$ is nowhere-zero then every edge must be in $E(F_i)$ for some $i$ and so $\bigcup_{\;i=1}^{\;p} F_i = G$ as required. Moreover, each component $f_i$ defines a nowhere-zero $\Z_2$-flow on $F_i$. Thus we must have $f(e)_i = 1$ for all $e \in F_i$ and so the number of edges incident to each vertex of $f_i$ must be even, since $f_i$ must satisfy Kirchhoff's Law. Hence we also have that each $F_i$ is an even subgraph as required.
\end{proof} 

In order to the prove the 8-Flow Theorem we will also require a corollary to the following well-known result of Tutte \cite{T4} and Nash-Williams \cite{NW}.

{\thm Let $k \geq 0$ be an integer. A graph $G=(V,E)$ has $k$ edge-disjoint spanning trees if and only if for any partition $\pi$ of $V$, the number of edges whose ends lie in two different parts of $\pi$ is at least $k(|\pi| - 1)$, where $|\pi|$.}

{\cor\label{cor:trees} Every $2k$-edge-connected graph has $k$ edge-disjoint spanning trees.}
\begin{proof}
Derive the corollary from the theorem.
\end{proof}

In the proof of the 8-Flow Theorem we use Corollary \ref{cor:trees} in order to find spanning trees which we then use to construct even subgraphs so that we can apply Proposition \ref{props:evensub}. The next lemma provides the construction we will use.

{\lem\label{algo} Let $T = (V, E_T)$ be a spanning tree of $G = (V,E)$. Then there exists $A \subseteq E_T$ such that $F = (V, (E \setminus E_T) \cup A)$ is an even subgraph of $G$.}
\begin{proof}
We describe an algorithm to find $A$. Begin with $U = E \setminus E_T$, $A = \emptyset$ and $T' = T$. At every iteration we find a leaf (a vertex of degree 1) $v$ of $T$. If the degree of $v$ in $(V,U)$ is odd then we add the edge of $T$ incident to $v$ to $A$. We then set $U = U \cup A$. (If $v$ has even degree in $(V,U)$ then $U$ is unchanged.) Next we set $T' = T' - v$ and repeat the process. When $T'$ has exactly one vertex left, we have that all other $v \in V$ have even degree in $(V,U)$. By the Handshaking Lemma \ref{lem:hands} the total sum of degrees must be even and thus the last vertex must also have even degree in $(V,U)$. Then we have $A \subseteq E_T$ such that, for $U = (E \setminus E_T) \cup A$, $F = (V,U)$ is an even subgraph of $G$.
\end{proof}

We now have all the results and techniques required to complete the proof of the 8-Flow Theorem.

{\thmn[\ref{eightflow}] Every bridgeless graph admits a nowhere-zero 8-flow.}
\begin{proof}
Suppose for contradiction that $G= (V,E)$ is a bridgeless graph that does not admit a nowhere-zero 8-flow. In Lemma \ref{sey2.1}, we will show that a minimal counterexample is simple, cubic and three-connected (in fact we prove the stronger result that this is true for all $k > 2$). In particular, then, we may assume that $G$ is three-edge-connected. Now, form $G' = (V,E')$ by duplicating every edge of $G$. Thus $G'$ is a 6-edge-connected graph and so by Corollary \ref{cor:trees} has 3 edge-disjoint spanning trees $T_1', T_2'$ and $T_3'$. Define 
\begin{align*}
E(T_i) &= \{e \in E: e \text{ or the duplicate of } e \text{ is in } E(T_i')\}
\intertext{and}
V(T_i) &= \{v \in V : \exists e \in E(T_i) \text{ incident to } v\}.
\end{align*}
Then the $T_i$ are spanning trees of $G$ and for all $e \in E$ there is some $i$ such that $e \not\in E(T_i)$. By Lemma \ref{algo} we can find $A_i$ such that $F_i = (V, (E \setminus E(T_i)) \cup A_i)$ is even. Since each edge is in $E \setminus E(T_i)$ for some $i$, then $G = F_1 \cup F_2 \cup F_3$ and so, by Proposition \ref{props:evensub}, $G$ has a nowhere-zero 8-flow.
\end{proof}