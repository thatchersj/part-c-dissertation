\section{Planar Graphs}
An \emph{embedding} of a graph $G$ in the plane is a drawing of the graph in the plane. Formally this amounts to a point $\bar{v}$ in the plane representing each vertex $v$ of the graph, and for each edge $e=xy$ a continuous curve $\bar{e} : [0,1] \to \R^2$ between $\bar{x}$ and $\bar{y}$ such that for all $t \in (0,1)$ and $v \in V$ we have $\bar{e}(t) \neq \bar{v}$. We also insist that the curves $\bar{e}$ are simple, that is $\bar{e}(t_1) \neq \bar{e}(t_2)$ for any distinct $t_1,t_2 \in (0,1)$.

\input{figk4}

We say a graph $G$ is \emph{planar} if it can be drawn in the plane so that no two edges cross one another. In the formal framework introduced above this is equivalent to saying that $G$ has an embedding such that for any two distinct edges $e_1$ and $e_2$ we have $\bar{e}_1(t_1) \neq \bar{e}_2(t_2)$ for any choice of $t_1,t_2 \in (0,1)$. As Figure \ref{k4} shows, a planar graph can have non-planar embeddings, and thus whether a given graph is planar or not may not be immediately obvious. To show that a graph is non-planar it is of course insufficient to say that we can't find a planar embedding, we must come up with some more quantitative measure of planarity.

For the remainder of this chapter, when we say planar graph it will be assumed that we are considering a planar embedding of the graph. The edges and vertices of the embedding are the points and curves as described above. We will also want to consider \emph{faces} of the graph, areas of the plane bounded by edges. In this definition we include the unbounded area outside the graph.

\begin{exmp}
Let $G$ be the following planar graph.
\begin{figure}[H]
\centering\begin{tikzpicture}
\SetGraphUnit{1.4}
\SetVertexMath
\tikzset{EdgeStyle/.append style = {bend left = 0}}
\tikzset{VertexStyle/.append style = {minimum size = 20pt, inner sep=0}}
\Vertex{A}
\SOWE(A){B}
\SOEA(B){D}
\NOEA(D){E}
\NOEA(E){C}
\SOEA(E){F}
\SetGraphUnit{1}
\SO[empty](D){bot}
\NO[empty](A){top}
\SetGraphUnit{4}
\EA[empty](bot){BR}
\EA[empty](top){TR}
\SetGraphUnit{2.5}
\WE[empty](bot){BL}
\WE[empty](top){TL}
\Edge(A)(B)
\Edge(A)(C)
\Edge(A)(E)
\Edge(B)(D)
\Edge(B)(E)
\Edge(C)(E)
\Edge(D)(E)
\Edge(E)(F)
\Edge(TL)(TR)
\Edge(TR)(BR)
\Edge(BR)(BL)
\Edge(BL)(TL)
\node (a) at (-1.7,0) {$F_1$};
\node (a) at (0,-0.85) {$F_2$};
\node (a) at (0,-1.95) {$F_3$};
\node (a) at (1.4,-0.55) {$F_4$};
\end{tikzpicture}\vspace*{-10pt}
\end{figure}
Then $G$ has four faces, which we have labelled $F_1,F_2,F_3$ and $F_4$.
\end{exmp}

A classical result in the theory of planar graphs is Euler's Formula, which links the number of vertices, edges and faces of a planar graph:

{\thm[Euler's Formula] Suppose that $G$ is a planar graph with $v$ vertices, $e$ edges and $f$ faces. Then}
$$v-e+f=2.$$

We do not give a proof of Euler's Formula here, but merely observe that it gives us a quantitative condition for graph planarity. From Euler's Formula it is possible to construct proofs by contradiction of then non-planarity of $K_5$, the complete graph on five vertices, and $K_{3,3}$, the complete bipartite graph with three elements in each partite set. In 1930, Kuratowski \cite{Kura} proved the following theorem, characterizing all non-planar graphs in terms of $K_5$ and $K_{3,3}$.

{\thm[Kuratowski's Theorem] A graph $G$ is planar if and only if it contains no subdivision of $K_5$ or $K_{3,3}$.\\}

\input{subdiv}

The \emph{subdivision} of an edge $e = xy$ results in a graph with a new vertex $w$ and with edges $e_1 = xw$ and $e_2 = wy$ replacing $e$, as shown in Figure \ref{subdiv}. We say that a graph $H$ is a subdivision of another graph $G$ if it can be obtained by repeatedly subdividing edges of $G$.

Figure \ref{pete} shows the subdivision of $K_{3,3}$ contained as a subgraph of Petersen graph. Kuratowski's Theorem then implies that the Petersen graph is non-planar.

\input{pete}

Before we move to prove the duality theorem, we introduce graph colouring. There are three main variants of graph colouring: vertex colouring, edge colouring and face colouring. We shall only be concerned with face colouring. For a planar graph $G$ and a set $F$ of the faces of $G$, $\chi:F \to \{1, \ldots, k\}$ is a \emph{proper $k$-colouring} if no two adjacent faces receive the same colour. In 1890, Heawood \cite{5col} proved the Five Colour Theorem, which states that every planar graph has a proper $5$-colouring. The stronger Four Colour Theorem - that every planar graph has a proper $4$-colouring - was first conjectured in the mid-nineteenth century, but was not proved until 1976 when Appel and Haken \cite{4col} presented their famous computer assisted proof. 