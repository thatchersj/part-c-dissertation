\chapter{Introduction}
\chaps{Introduction}

A \emph{graph} $G$ is a pair $(V,E)$ consisting of a set $V$ of \emph{vertices} and a set $E$ of \emph{edges} $e=v_1v_2$ joining vertices $v_1$ and $v_2$. We allow our graphs to have loops and multiple edges. In this essay we shall be discussing flows on graphs, an important notion for which will be the direction in which flow is travelling along an edge. For this purpose we define an \emph{orientation} on $G$ as a map $(\iota,\tau):E \times E \to V \times V$ which we interpret as telling us that the edge $e$ is directed from $\iota(e)$ to $\tau(e)$. We call a graph with a specified orientation $\Omega$ an \emph{oriented graph}, and write $(G,\Omega)$ to denote the graph with its orientation. Having fixed an orientation for $G$, for a vertex $v \in V$ we write $\delta^+(v)$ for the set edges of $G$ directed out of $v$: 
$$\delta^+(v) = \{ e \in E : \iota(e) = v\}$$ 
Similarly, we denote by $\delta^-(v)$ the edges of $G$ directed into $v$. The \emph{degree} of a vertex $v$ is the total number of edges going in and out of $v$, we write $$d_v = |\delta^+(v)| + |\delta^-(v)|$$ and note that this quantity is invariant of the choice of orientation. A classical result relating degree to the number of edges is the Handshaking Lemma:

{\lem[Handshaking Lemma]\label{lem:hands} For a graph $G = (V,E)$, $$|E| = \frac{1}{2} \sum_{v \in V} d_v.$$}
\begin{proof}
Fix an orientation $\Omega$ of $G$. Then for every edge $e \in E$ there are unique $v,w \in V$ such that $e \in \delta^+(v)$ and $e \in \delta^-(w)$. Thus when we add up the degrees of all of the vertices each edge contributes twice to the sum, once for $v$ and once for $w$. Therefore, the degree sum is twice the number of edges, and the result follows. (This lemma is so called as it observes that just as there are two hands in every handshake, there are two vertices on every edge.)
\end{proof}

For $S \subset V$ a subset of the vertices of $G$, we define $G\setminus S$ to be the graph obtained from $G$ by deleting all of the vertices in $S$ and all of the edges incident to them. If $e = xy$ is an edge of $G$, then we define $G-e$ to be the graph obtained by deleting the edge $e$. We also define $G/e$ to be the graph obtained by deleting $e$ and identifying each of its endpoints $x$ and $y$ in a new point $z$. This process, shown in Figure \ref{contract}, is called \emph{contracting} the edge $e$. Formally, $G/e$ is the graph with vertex set
$$V(G/e) = \left( V(G) \setminus \{x,y\} \right) \cup \{z\}$$
where $z$ is a new vertex (i.e. $z \not\in V(G))$, and edge set
$$E(G/e) = \{ uv \in E(G) : u,v \not\in \{x,y\} \} \cup \{ vz : \exists u \in \{x,y\} : uv \in E(G)  \}.$$

\input{contraction}

For an (additive) abelian group $H$ we define an $H$-\emph{flow} on an oriented graph $(G,\Omega)$ to be a map $f:E \to H$ such that for all $v \in V$:
\begin{equation}\label{kirch}
\sum_{e \in \delta^+(v)} f(e) = \sum_{e \in \delta^-(v)} f(e).
\end{equation}

Equation \ref{kirch} is known as \emph{Kirchhoff's Law}. We will also want to consider the net flow out of a vertex $v$ due to such a map $f$, for this we write $$\hat{f}(v) = \sum_{e \in \delta^+(v)} f(e) - \sum_{e \in \delta^-(v)} f(e).$$ 
Then Kirchhoff's Law is equivalent to $\hat{f}(v) = 0$ for all $v \in V$. For $U \subseteq V$ we define $\hat{f}(U)$ as the net flow along edges out of $U$. Note that $\hat{f}(U) = \sum_{u \in U} \hat{f}(u)$ since edges between vertices in $U$ are counted twice with opposite signs and thus contribute zero to the overall sum. 
\newpage
The study of flows on graphs has a number of important applications. For example, flows can be used to model the flow of traffic on a road network, of oil along transcontinental pipelines, or of data across network infrastructure. An important concept in these areas is the \emph{capacity} of each edge in a network, that is the maximum amount of flow allowed along a given edge - a road can take only so many cars, a pipeline only so much oil, and a network cable only so much data. Designing flows subject to capacity restrictions and further constraints such as the relative costs, safety or efficiency of using different possible routes, provides interesting problems in communication and optimisation theory. In this dissertation though we will be focussing upon flows in an abstract, graph theoretic setting. In particular we will be asking when certain special types of $H$-flows can exist for a particular group $H$ on a particular graph $G$. 

\input{exflows}

Figure \ref{exflow} gives examples of $H$-flows on a graph for $H = S_3$ and $H = \Z_2$. The $S_3$-flow is an example of a specific type of flow which we shall be focussing upon throughout the course of this dissertation: flows for which none of the edges are given the value $0 \in H$. Formally, a \emph{nowhere-zero} $H$-\emph{flow} on an oriented graph $(G,\Omega)$ is an $H$-flow $f$ such that $f(e) \neq 0$ for all $e \in E$. In Chapter 2 we will show that, for a general abelian group $H$, a nowhere-zero $H$-flow exists if and only we can construct a nowhere-zero integer flow ($\Z$-flow) such that the magnitude of the flow along any edge is strictly less than the size of $H$. In the integer flow case we say that a flow $f$ on an oriented graph $(G,\Omega)$ is a \emph{nowhere-zero} $k$\emph{-flow} for an integer $k$ if for all $e \in E$ we have $0 < |f(e)| < k$. 

Once we have confirmed that a nowhere-zero flow exists, a natural follow up question would be  to ask how many such flows there are. For a graph $G$ and an abelian group $H$, we denote by $\Phi(G,H)$ the number of nowhere-zero $H$-flows on $G$. Our definition of a flow is dependent upon an orientation of $G$. The following theorem tells us that for $\Z$-flows the existence of a nowhere-zero flow is in fact independent of the choice of orientation. In particular, then, $\Phi(G,H)$ is well-defined.

{\thm\label{orient} Let $\Omega$ and $\Theta$ be orientations of a graph $G$. Then there is a nowhere-zero $\Z$-flow on $(G,\Omega)$ if and only if there is a nowhere-zero $\Z$-flow on $(G,\Theta)$.}
\begin{proof}

Observe that a flow of $+1$ out of a vertex $v$ and a flow of $-1$ into $v$ contribute the same amount to the net flow out of $v$. Thus given a nowhere-zero $\Z$-flow on $(G,\Omega)$ we can obtain a nowhere-zero flow on $(G, \Theta)$ by negating the flow along all edges on whose orientations $\Omega$ and $\Theta$ disagree.
\end{proof}


We stated Theorem \ref{orient} above in terms of $\Z$-flows, however, we can see quickly that the same result will also apply to $\Z_k$- and $k$-flows since both $\Z_k$ and the set $\{\pm 1, \ldots, \pm(k-1)\}$ are closed under multiplication by $-1$. As a consequence, we will not generally need to worry about orientations in our subsequent discussions of flows. We will therefore talk of a flow on a graph $G$ meaning a flow on $G$ given some fixed, but arbitrary, orientation.\\

$***$ Brief summary of what is to come

\newpage